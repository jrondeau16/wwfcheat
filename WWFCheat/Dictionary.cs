using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WWFCheat
{
	public class Dictionary
	{
		public static int max_length;
		private static List<string> master_dictionary;
		public static string user_input;
		public static string[] user_letters;
		public static List<Word> accepted_words;
		private static Dictionary<char, int> letter_values;

		public struct Word{
			public Word(string Value, int Point_Value){
				value = Value;
				point_value = Point_Value;
			}
			public int point_value;
			public string value;
		}

		public Dictionary (string User_Input)
		{
			master_dictionary = new List<string> ();
			int max_length = 0;
			user_input = User_Input;
			accepted_words = new List<Word> ();
			letter_values = new Dictionary<char, int> ();
			Load_Dictionary ();
			Load_Letter_Values ();
		}

		private void Load_Dictionary(){
			try
			{
				using (StreamReader sr = new StreamReader(@"Dictionary.txt"))
				{
					string line;
					while((sr.ReadLine()) != null){
						line = sr.ReadLine(); 
						master_dictionary.Add(line);
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("The file could not be read:");
				Console.WriteLine(e.Message);
			}
		}

		private static void Parse_UserInput(){
			user_letters = user_input.Split (',');
			max_length = user_letters.Length;
		}

		private static bool Check_LetterOverflow(string word){
			List<string> remaining_letters = new List<string> ();
			bool letter_overflow = false;
			char[] word_letters = word.ToCharArray();

			foreach (string s in user_letters) {
				remaining_letters.Add (s);
			}

			foreach (char s in word_letters) {
				if (remaining_letters.Contains (s.ToString ())) {
					remaining_letters.Remove (s.ToString ());
				} else {
					letter_overflow = true;
					return letter_overflow;
				}
			}

			return letter_overflow;
		}

		private static void Add_Word(string word, int point_value){
			Word word_struct = new Word (word, point_value);
			accepted_words.Add (word_struct);
		}

		public void Test_Input(){
			Parse_UserInput ();
			for(int i = 0; i < master_dictionary.Count-1; i++) {
				string word = master_dictionary.ElementAt (i);
				if (Check_LetterOverflow (word)) {
					continue;
				} else {
					int point_value = Point_Word (word);
					Add_Word (word, point_value);
				}
			}
		}

		private void Load_Letter_Values(){
			letter_values.Add ('a', 1);
			letter_values.Add ('b', 4);
			letter_values.Add ('c', 4);
			letter_values.Add ('d', 2);
			letter_values.Add ('e', 1);
			letter_values.Add ('f', 4);
			letter_values.Add ('g', 3);
			letter_values.Add ('h', 3);
			letter_values.Add ('i', 1);
			letter_values.Add ('j', 10);
			letter_values.Add ('k', 5);
			letter_values.Add ('l', 2);
			letter_values.Add ('m', 4);
			letter_values.Add ('n', 2);
			letter_values.Add ('o', 1);
			letter_values.Add ('p', 4);
			letter_values.Add ('q', 10);
			letter_values.Add ('r', 1);
			letter_values.Add ('s', 1);
			letter_values.Add ('t', 1);
			letter_values.Add ('u', 2);
			letter_values.Add ('v', 5);
			letter_values.Add ('w', 4);
			letter_values.Add ('x', 8);
			letter_values.Add ('y', 3);
			letter_values.Add ('z', 10);
		}

		private static int Point_Word(string word){
			char[] characters = word.ToCharArray();
			int value = 0;
			foreach (char character in characters) {
				value += letter_values [character];
			}
			return value;
		}

		public string Output_Words(){
			List<Word> SortedList = accepted_words.OrderByDescending(o=>o.point_value).ToList();
			string output = "";
			foreach (Word word in SortedList) {
				string k = String.Format("{0} \t \t {1}", 
				                         word.value, word.point_value);
				output += k + Environment.NewLine;
			}
			return output;
		}

		public void Reset(){
			user_input = null;
			user_letters = null;
			user_input = null;
			user_letters = null;
			accepted_words = null;
		}

	}
}